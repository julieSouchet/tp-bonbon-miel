// analog clock
const hoursHand = document.getElementById('hoursHand');
const minutesHand = document.getElementById('minutesHand');
const secondsHand = document.getElementById('secondsHand');
// images
const links = document.getElementsByClassName('horloge-link');
const pictures = document.getElementsByClassName('horloge-img');
let previousHour;
// clock size
const rayonHorloge = '-40vmin';

// to read the hour, minutes, seconds
let h; let m; let s;

/**
 * Met à jour les aiguilles (vue)
 */
function updateAnalogClock() {
  // hours
  const hoursToDegrees = (h)*30 - 90 + m/2;
  hoursHand.style.transform = `rotate(${hoursToDegrees}deg)`;
  // minutes
  const minutesToDegrees = m*6 - 90 + s/10;
  minutesHand.style.transform = `rotate(${minutesToDegrees}deg)`;
  // seconds
  const secondsToDegrees = s*6 - 90;
  secondsHand.style.transform = `rotate(${secondsToDegrees}deg)`;

  // image highlighting
  if (h !== previousHour) {
    previousHour = h === 0 ? links.length-1 : h-1;
    links[previousHour%12].classList.remove('active');
    links[h%12].classList.add('active');
    previousHour = h;
  }
}

/**
 * Met à jour l'heure (modèle)
 */
function setDate() {
  const date = new Date();
  h = date.getHours();
  m = date.getMinutes();
  s = date.getSeconds();

  // update HTML
  updateAnalogClock();
}

/**
 * Initialise l'horloge.
 */
export default function init() {
  setDate();
  setInterval(setDate, 1000);

  // positionne les photos
  for (let i = 0; i < links.length; i++) {
    const link = links[i];
    const picture = pictures[i];
    const deg = i*30;
    link.style.transform =
    `rotate(${deg}deg) translate(0, ${rayonHorloge})`;
    picture.style.transform = `rotate(${-deg}deg)`;
  }

  // highlight
  previousHour = -1;
};
