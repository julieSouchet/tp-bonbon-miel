import 'reset-css';
import './assets/css/style.css';
import './assets/css/header.css';
import './assets/css/lightbox.css';
import './assets/css/accueil.css';
import './assets/css/biographie.css';
import './assets/css/horloge.css';
import './assets/css/regime.css';
import './assets/css/galerie.css';
// lightbox
import 'fslightbox';
import init from './date.js';

document.addEventListener('DOMContentLoaded', () => {
  init();

  // scrolling
  const header = document.getElementsByTagName('header')[0];
  let scrolling;
  document.addEventListener('scroll', () => {
    scrolling = true;
  }, {passive: true});

  setInterval(() => {
    if (scrolling) {
      scrolling = false;

      // sticky
      if (document.documentElement.scrollTop >
        document.documentElement.clientHeight) {
        header.style.position = 'fixed';
      } else {
        header.style.position = 'relative';
      }

      // progress
      const percentage = document.documentElement.scrollTop /
      (document.documentElement.scrollHeight -
        document.documentElement.clientHeight) * 100;
      // eslint-disable-next-line max-len
      header.style.backgroundImage = `linear-gradient(to bottom right, lavender ${percentage}%, lightsteelblue ${percentage}%)`;
    }
  }, 100);
});
